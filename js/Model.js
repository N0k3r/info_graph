class Model {
    constructor(Type, life, power) {
        this.Mesh = ProtoTypes[Type].Mesh.clone()
        this.life = (life == undefined) ? 1 : life
        this.power = (power == undefined) ? 1 : power
        this.pov = false
        this.alive = false
        this.MovementSpeed = 0.25
        if (Type != ObjectType.Player && Type != ObjectType.Rocket) {
            this.Mesh.rotateY(THREE.Math.degToRad(180))
            this.direction = 1
        } else {
            if (Type == ObjectType.Player) {
                this.direction = -1
                this.isPlayer = true
            }
        }
        this.Type = Type
        this.interval = undefined
        this.H_Mv_Interval = undefined
        this.ShootingInterval = undefined
    }

    die() {
        if(this.pov) this.POV() 
        if (this.Mesh.type == "Group") {
            this.Mesh.children.forEach(elm => {
                elm.geometry.dispose()
                elm.material.dispose()
            })
        } else {
            this.Mesh.geometry.dispose()
            this.Mesh.material.dispose()
        }
        scene.remove(this.Mesh)
        this.alive = false
        this.ClearIntervals()
    }

    live(x, y, z) {
        this.Mesh.position.x = (x == undefined) ? 0 : x
        this.Mesh.position.y = (y == undefined) ? 0 : y
        this.Mesh.position.z = (z == undefined) ? 0 : z
        scene.add(this.Mesh)
        this.alive = true
    }

    Shoot() {
        var Rocket = new Weapon(this.direction)
        Rocket.Mesh.position.set(this.Mesh.position.x, this.Mesh.position.y, this.Mesh.position.z)
        Rocket.launch(15)
    }

    launch(Speed) {
        if (!this.alive) {
            scene.add(this.Mesh)
            this.alive = true
        }
        this.interval = setInterval(function (Obj) {
            if (Player != null || Player != undefined) {
                if (Obj.Mesh.position.z <= 30 && Obj.Mesh.position.z >= -30) {
                    Obj.Mesh.position.z += Obj.direction
                    if (Obj.direction != Player.direction) {
                        if (Obj.Mesh.position.z <= Player.Mesh.position.z + 1 && Obj.Mesh.position.z >= Player.Mesh.position.z - 1
                            &&
                            Obj.Mesh.position.x <= Player.Mesh.position.x + 5 && Obj.Mesh.position.x >= Player.Mesh.position.x - 5
                        ) {
                            Player.life -= 1
                            Obj.die()
                            clearInterval(Obj.interval)
                        }
                    } else {
                        for (let i = 0; i < Enemies.kamikazes.length; i++) {
                            if (Obj.Mesh.position.z <= Enemies.kamikazes[i].Mesh.position.z + 5 && Obj.Mesh.position.z >= Enemies.kamikazes[i].Mesh.position.z - 5
                                &&
                                Obj.Mesh.position.x <= Enemies.kamikazes[i].Mesh.position.x + 5 && Obj.Mesh.position.x >= Enemies.kamikazes[i].Mesh.position.x - 5
                            ) {
                                Enemies.kamikazes[i].life -= 1
                                Obj.die()
                                Game.Score += 10
                                clearInterval(Obj.interval)
                            }
                        }
                        
                        for (let i = 0; i < Enemies.Shooters.length; i++) {
                            if (Obj.Mesh.position.z <= Enemies.Shooters[i].Mesh.position.z + 2 && Obj.Mesh.position.x >= Enemies.Shooters[i].Mesh.position.z -2
                                &&
                                Obj.Mesh.position.x <= Enemies.Shooters[i].Mesh.position.x + 5 && Obj.Mesh.position.x >= Enemies.Shooters[i].Mesh.position.x - 5
                            ) {
                                Enemies.Shooters[i].life -= 1                                
                                Obj.die()
                                Game.Score += 50
                                clearInterval(Obj.interval)
                            }
                        }
                    }

                }
                else {
                    if (!Obj.isPlayer) {
                        Obj.die()
                        clearInterval(Obj.interval)
                    }
                }
            }

        }, Speed, this)
    }

    POV() {
        if (this.pov) {
            this.Mesh.remove(camera)
            camera.position.set(0, 80, 0)
            camera.lookAt(0, 0, 0)
        } else {
            this.Mesh.add(camera)
            camera.position.set(0, 3, -7)
            camera.lookAt(0, 0, -100)
        }
        this.pov = !this.pov
    }

    MoveRight() {
        if (this.Mesh.rotation.z <= -2.35)
            this.Mesh.rotation.z += 0.1
        this.Mesh.position.x += this.MovementSpeed
    }

    MoveLeft() {
        if (this.Mesh.rotation.z >= -3.92)
            this.Mesh.rotation.z -= 0.1
        this.Mesh.position.x -= this.MovementSpeed
    }

    MoveUp() {
        this.Mesh.position.z -= this.MovementSpeed
    }

    MoveDown() {
        this.Mesh.position.z += this.MovementSpeed
    }

    AutoMoveDown() {
        this.interval = setInterval(function (Obj) {
            if(Player == null) clearInterval(Obj.interval)
            if (Obj.Mesh.position.z <= 35 && Obj.Mesh.position.z >= -35 && Obj.life != 0) {
                Obj.Mesh.position.z += 0.1
                if (
                    Obj.direction != Player.direction
                    &&
                    Obj.Mesh.position.z <= Player.Mesh.position.z + 3 && Obj.Mesh.position.z >= Player.Mesh.position.z - 3 
                    &&
                    Obj.Mesh.position.x <= Player.Mesh.position.x + 5 && Obj.Mesh.position.x >= Player.Mesh.position.x - 5
                ) {
                    if(Obj.Type == ObjectType.Sheild){
                        Player.life += 1
                    }
                    else if(Obj.Type == ObjectType.Coin) {
                        Game.Score += 50
                    }
                    else{
                        Player.life -= 1
                    } 
                    Obj.life -= 1
                }
            }
            else
                if (!Obj.isPlayer) {
                    Obj.die()
                    clearInterval(Obj.interval)
                }
        }, 10, this)
    }

    AutoShoot(Speed) {
        this.ShootingInterval = setInterval((Obj) => {
            this.Shoot()
        }, Speed, this);
    }

    StopShooting() {
        if (this.ShootingInterval != undefined)
            clearInterval(this.ShootingInterval)
    }

    ClearIntervals() {
        if (this.H_Mv_Interval != undefined) clearInterval(this.H_Mv_Interval)
        if (this.ShootingInterval != undefined) clearInterval(this.ShootingInterval)
        if (this.interval != undefined) clearInterval(this.interval)
    }

    MoveHorizontal(Speed) {
        var Direction = PosOrNeg()
        this.H_Mv_Interval = setInterval((Obj) => {
            if (Direction < 0) {
                if (Obj.Mesh.position.x > -50)
                    Obj.Mesh.position.x -= 1
                else
                    Direction = Direction * -1
            } else {
                if (Obj.Mesh.position.x < 50)
                    Obj.Mesh.position.x += 1
                else
                    Direction = Direction * -1
            }


        }, Speed, this);
    }

    StopHorizontalMoving() {
        if (this.H_Mv_Interval != undefined)
            clearInterval(this.H_Mv_Interval)
    }
}

class Weapon extends Model {
    constructor(direction) {
        super(ObjectType.Rocket, 1, 1)
        this.direction = direction
        if (this.direction >= 0) this.Mesh.rotateY(THREE.Math.degToRad(180))
    }
}