var renderer = new THREE.WebGLRenderer()
var scene = new THREE.Scene()
var camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000)
var light = new THREE.PointLight()
var Bg_Texture = new THREE.TextureLoader().load("./images/stars.jpg")
// var controls, Player, Enm
var ScrollingIndex = 1

var gui = new dat.GUI();


function init() {
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    Bg_Texture.wrapS = THREE.RepeatWrapping
    Bg_Texture.wrapT = THREE.RepeatWrapping
    scene.background = Bg_Texture

    light.position.set(0, 100, 0)
    scene.add(light)

    camera.position.set(0, 80, 0)
    camera.lookAt(0, 0, 0)
    scene.add(camera)

    StartGame()

    if(Player != null || Player != undefined){
        gui.add(Player, "life").listen()
        gui.add(Game, "Score").listen()
    }

    animate()
}

function animate() {
    if (Player != null || Player != undefined) {        
        renderer.render(scene, camera)
        requestAnimationFrame(animate)

        Bg_Texture.offset.set(0, ScrollingIndex / 1000)
        ScrollingIndex += 5

        if (!Player.alive || Player.life <= 0) {
            Player.die()
            EndGame()
        }
        else{
            if (ActiveKeys.right) Player.MoveRight()
            if (ActiveKeys.up) Player.MoveUp()
            if (ActiveKeys.left) Player.MoveLeft()
            if (ActiveKeys.down) Player.MoveDown()
            if (ActiveKeys.shoot) Player.Shoot()
    
            for (let i = 0; i < Enemies.kamikazes.length; i++) {
                if (!Enemies.kamikazes[i].alive || Enemies.kamikazes[i].life <= 0) {
                    Enemies.kamikazes[i].die()
                    Enemies.kamikazes.splice(i, 1)
                }
    
            }
    
            for (let i = 0; i < Enemies.Shooters.length; i++) {
                if (!Enemies.Shooters[i].alive || Enemies.Shooters[i].life <= 0) {   
                    Enemies.Shooters[i].die()
                    Enemies.Shooters.splice(i, 1)
                }
            }
        }
        
    }else{
        let GameOver = document.getElementById("tudo");
        GameOver.removeAttribute("hidden"); 
        document.getElementById("Score").innerHTML = "Score : " + Game.Score + "    , REPLAY ?"
    }
}

init()
var LodingDiv = document.getElementById("L")
LodingDiv.parentNode.removeChild(LodingDiv);